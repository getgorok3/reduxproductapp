
import { applyMiddleware, compose, createStore , combineReducers} from 'redux'
import { routerMiddleware , connectRouter} from 'connected-react-router'
import { createMemoryHistory } from 'history'
import logger from 'redux-logger'
import ImageReducer from './ImagesReducer'
import UserInfoReducer from './UserInfoReducer'
import TestReducer from './TestReducer'


const reducers = (history)=>combineReducers({   
    images: ImageReducer,
    userInfo: UserInfoReducer,
    testReducer: TestReducer,
    router: connectRouter(history)
})
export const history = createMemoryHistory()

// const store = createStore(reducers);
// export default store;
export const store = createStore(
    reducers(history),
    compose(
    applyMiddleware(
        routerMiddleware(history),
        logger
    )
)
)
