export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return imageReducer(null, action)
        case 'EDIT_IMAGE':
            return imageReducer(null, action)



        case 'KEEP_IMAGE':
            return imageReducer(null, action)
        case 'CLEAR':
            return imageReducer(null, action)
        default:
            return state
    }
}
const imageReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return {
                image: action.image,
                imageName: action.imageName,
                addStatus: true
            }
        case 'EDIT_IMAGE':
            return {
                ...state,
                image: action.image,
                imageName: action.imageName,
                imIndex: action.imIndex
            }
        case 'KEEP_IMAGE':
            return {
                image: action.image,
                imageName: action.imageName,
                imIndex: action.imIndex
            }
        case 'CLEAR':
            return {}

        default:
            return state
    }
}