export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_USER':
            return [...state, userReducer(null, action)]
        case 'EDIT_USER':
            return state.map((each, index) => {
                if (each.username === action.username) {
                    return userReducer(each, action)

                }
                return each
            })
      
        default:
            return state
    }
}
// for managing just one person
const userReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return {
                username: action.username,
                firstname: action.firstname,
                lastname: action.lastname,
                password: action.password
            }
        case 'EDIT_USER':
            return {
                ...state,
                firstname: action.firstname,
                lastname: action.lastname
            }

        default:
            return state
    }
}