export default (state = [], action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return [...state, productReducer(null, action)]
        default:
            return state
    }
}

const productReducer = (state = {}, action) => {
    switch (action.type) {
        case 'ADD_PRODUCT':
            return {
                imageUrl: action.imageUrl,
                productName: action.productName,
                price: action.price,
                star: action.star
            }
    }
}

