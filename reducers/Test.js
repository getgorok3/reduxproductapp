import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Alert, ImageBackground, FlatList, input } from 'react-native';
import { connect } from 'react-redux'
import {
    Container, Row3, MenuText, SubMenu, Topic, Row4, ProfileImageFrame,
    Content, NormalColumn, Header, ColumnEditPage, Row5, LogInImage, UserInputBox, UserInput
} from '../styledComponent'
class Test extends Component {
    state = {
        imageUrl: '',
        productName: '',
        price: '',
        star: ''
    }
    save = () => {
        console.log('save', this.props)
        this.props.addProduct(this.state.imageUrl,
            this.state.productName, this.state.price, this.state.star)


    }
    render() {
        const { testReducer } = this.props
        console.log('Test', this.props)
        return (
            <Row4 style={{ backgroundColor: 'white', marginTop: '5%', marginBottom: '5%' }}>
                <ColumnEditPage style={{ height: 600 }}>
                    <Row3>
                        <UserInputBox style={{ marginLeft: '8%', marginTop: '10%' }}>
                            <UserInput clear placeholder="Url"

                                onChangeText={(imageUrl) => this.setState({ imageUrl })}></UserInput>
                            <UserInput clear placeholder="Name"

                                onChangeText={(productName) => this.setState({ productName })} ></UserInput>
                            <UserInput clear placeholder="Price"

                                onChangeText={(price) => this.setState({ price })} ></UserInput>
                            <UserInput clear placeholder="Star"

                                onChangeText={(star) => this.setState({ star })} ></UserInput>
                        </UserInputBox>
                    </Row3>
                    <Row3>
                        <SubMenu type="primary" style={{
                            width: '80%',
                            backgroundColor: '#ff4d4d', marginBottom: '5%',
                            marginLeft: '10%'
                        }} onPress={this.save}>
                            <Text style={{ fontSize: 25 }}>Edit</Text></SubMenu>


                    </Row3>
                    {testReducer.map((each, index) => {
                        return (
                            <Text key={index}>{each.imageUrl}Name: 
                            {each.productName} price: {each.price} star: {each.star}</Text>
                        )
                    })}
                </ColumnEditPage>
            </Row4>

        )
    }
}
const mapStateToProps = (state) => {
    return {
        testReducer: state.testReducer,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addProduct: (imageUrl, productName, price, star) => {
            dispatch({
                type: 'ADD_PRODUCT',
                imageUrl: imageUrl,
                productName: productName,
                price: price,
                star: star
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps
)(Test) 