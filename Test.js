import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Button } from '@ant-design/react-native';
import { connect } from 'react-redux'
class Test extends Component {
  render() {
      console.log(this.props)
    const { images, userInfo,addUser,addImage } = this.props
    return (
        <View style={{ flex: 1, alignItems: 'center', }}>
          <TouchableOpacity onPress={() => {
            addUser('thanakan@gmail.com', 'Thanakan', 'Thanakwang');
          }}>
            <Text>Add User </Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => {
            addImage('https://animals.sandiegozoo.org/sites/default/files/2016-08/category-thumbnail-mammals_0.jpg',
              'Panda');
          }}>
            <Text>Add Image</Text>
          </TouchableOpacity>
          <Text>
            {/* Latest user is: {userInfo[userInfo.length - 1]} */}
            gg
          </Text>
          <Text>
            {/* Latest image is: {images[images.length - 1].image} */}
            gg
          </Text>
          <Button type="primary">primary</Button>
        </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userInfo: state.userInfo,
    images: state.images
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addUser: (username, firstname, lastname) => {
      dispatch({
        type: 'ADD_USER',
        username: username,
        firstname: firstname,
        lastname: lastname
      })
    },
    addImage: (url, imageName) => {
      dispatch({
        type: 'ADD_IMAGE',
        image: url,
        imageName: imageName,
        
      })
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps
)(Test) 