import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Login from './pages/LoginPage'
import Main from './pages/MainPage'
import Profile from './pages/ProfilePage'
import EditProfile from './pages/EditProfilePage'
import {ConnectedRouter } from 'connected-react-router'
import {store, history} from './reducers/AppStore.js'
import {Provider} from 'react-redux'
import Product from './pages/ProductPage'
import EditProduct from './pages/EditProductPage'
import AddProduct from './pages/AddProduct'

export default class Router extends Component {
    render() {
        return (
            // <Provider store={store}>
            <ConnectedRouter  history={history}>
                <Switch>
                    <Route exact path="/" component={Login} />
                    <Route exact path="/Main" component={Main} />
                    <Route exact path="/Profile" component={Profile} />
                    <Route exact path="/EditProfile" component={EditProfile} />
                    <Route exact path="/Product" component={Product} />
                    <Route exact path="/EditProduct" component={EditProduct} />
                    <Route exact path="/AddProduct" component={AddProduct} />
                </Switch>
            </ConnectedRouter >
            // </Provider>
        )
    }
}

