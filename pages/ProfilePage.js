import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {
    Container, Row3, MenuText, SubMenu, Topic, Row4,ProfileImageFrame,
    Content, NormalColumn, Header, ColumnEditPage, Row5, LogInImage
} from '../styledComponent'
import { connect } from 'react-redux'
class Profile extends Component {

    UNSAFE_componentWillMount() {

    }
    backToMain = () => {
        this.props.history.push('/Main')
    }
    goToEditPage = () => {
        this.props.history.push('/EditProfile')
    }
    render() {
        const { userInfo } = this.props
        return (
            <Container style={{ backgroundColor: '#ff8080' }}>
                <Header style={{ backgroundColor: '#ff1a1a' }}>
                    <SubMenu style={{ backgroundColor: '#ff1a1a', marginRight: 5 }}
                        onPress={this.backToMain}>
                        <Text style={{ color: 'white' }}>Back</Text></SubMenu>
                    <MenuText>My Profile</MenuText>
                </Header>


                <Row4 style={{ backgroundColor: 'white', marginTop: '5%',}}>

                    <ColumnEditPage style={{height: 500}}>
                        <ProfileImageFrame>
                            <LogInImage resizeMode="stretch" source={
                                {uri: 'https://lh3.googleusercontent.com/NDfTVvMTt2OylnpY7GwDTGynir_919yOZZ5ZxW8GO8_8bk-LpDstR8KKC-LywHQb61k' }
                                // require('./login.png')
                            } />
                        </ProfileImageFrame>
                        <Row3>
                            <NormalColumn>
                                <Topic>Username</Topic>
                                <Content>{userInfo[0].username}</Content>
                            </NormalColumn>
                        </Row3>

                        <Row3>
                            <NormalColumn>
                                <Topic>First name</Topic>
                                <Content>{this.props.userInfo[0].firstname}</Content>
                            </NormalColumn>
                        </Row3>

                        <Row3>
                            <NormalColumn>
                                <Topic>Last name</Topic>
                                <Content>{this.props.userInfo[0].lastname}</Content>
                            </NormalColumn>
                        </Row3>
                    </ColumnEditPage>
                </Row4>
                <Row5 style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: '5%',
                    marginRight: '5%',
                    

                }}>
                    <SubMenu type="primary" style={{ width: '80%',
                     backgroundColor: '#ff4d4d',   marginBottom: '5%', }} onPress={this.goToEditPage}>
                        <Text style={{fontSize: 25}}>Edit</Text></SubMenu>
                </Row5>
            </Container >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userInfo,
    }
}

export default connect(mapStateToProps, null
)(Profile) 