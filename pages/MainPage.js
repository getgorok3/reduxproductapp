import React, { Component } from 'react';
import { TouchableOpacity, Text, View, FlatList, Image, ListItem } from 'react-native';
import { Carousel } from '@ant-design/react-native';
import {
    Container, Row, Row2, MenuText, SubMenu, ImageFrameCS,
    NewImage, Footer, ImageFrame, Header, NewScrollView
} from '../styledComponent'
import { connect } from 'react-redux'


class Main extends Component {
    state = {
        ImageArr: [],

    }
    UNSAFE_componentWillMount() {
        console.log('open Main', this.props)
        if (this.props.images !== {}) {
            if (this.props.images.imIndex !== '' && this.props.images.imIndex !== undefined) {
                console.log('From Edit page')
                ImageFile = ImageFile.map((each, index) => {
                    if (index == this.props.images.imIndex) {
                        each.url = this.props.images.image
                        each.name = this.props.images.imageName
                        console.log(each)
                    }
                    return each
                })
            } else if(this.props.images.addStatus === true){
                console.log('From Add product')

              ImageFile.push({
                    url: this.props.images.image,
                    name: this.props.images.imageName
                })
            
              this.props.clear()
            }

        }
        console.log('new arr', ImageFile)


    }

    goToProfilePage = () => {
        this.props.history.push('/Profile')
    }
    goToAddProductPage = () => {
        this.props.history.push('/AddProduct')
    }

    onPressImage = (url, name, index) => {
        console.log('cur', index)
        console.log('name', name)
        this.props.keepImage(url, name, index);
        console.log('after keeping', this.props)
        this.props.history.push('/Product')
    }
    logOut = () => {
        this.props.history.push('/')
    }
    render() {

        return (
            <Container>
                <Header style={{ backgroundColor: '#ff1a1a' }}>
                    <MenuText>List</MenuText>
                </Header>
                <NewScrollView >
                    <View>
                        <Carousel
                            selectedIndex={1}
                            autoplay
                            infinite
                        >
                            <ImageFrameCS >
                                <NewImage resizeMode="stretch" source={{ uri: ImageFile[0].url }} />
                            </ImageFrameCS>

                            <ImageFrameCS >
                                <NewImage resizeMode="stretch" source={{ uri: ImageFile[4].url }} />
                            </ImageFrameCS>

                            <ImageFrameCS >
                                <NewImage resizeMode="stretch" source={{ uri: ImageFile[6].url }} />
                            </ImageFrameCS>
                        </Carousel>
                    </View>
                    <Row2>
                    </Row2>

                    <Row2 >
                        <FlatList
                            style={{ marginLeft: '2.5%', marginRight: '2.5%' }}
                            data={ImageFile}
                            numColumns={2}
                            renderItem={({ item, index }) =>
                                <ImageFrame key={item.name}>
                                    <TouchableOpacity onPress={() => this.onPressImage(item.url, item.name, index)}>
                                        <NewImage resizeMode="stretch" source={{ uri: item.url }} />
                                    </TouchableOpacity>
                                </ImageFrame>
                            }
                        />
                    </Row2>
                </NewScrollView>
                <Footer style={{ backgroundColor: '#ff8080' }}>
                    <SubMenu style={{ backgroundColor: 'black' }} onPress={this.logOut}>
                        <Text style={{ color: 'white' }}>L</Text></SubMenu>
                    <TouchableOpacity onPress={this.goToAddProductPage} style={{ width: '78%' }}>
                        <MenuText>Add</MenuText>
                    </TouchableOpacity>
                    <SubMenu style={{ backgroundColor: 'black' }}
                        onPress={this.goToProfilePage}>
                        <Text style={{ color: 'white' }}>P</Text></SubMenu>
                </Footer>
            </Container>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        images: state.images,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        keepImage: (image, imageName, imIndex) => {
            dispatch({
                type: 'KEEP_IMAGE',
                image: image,
                imageName: imageName,
                imIndex: imIndex
            })
        },
        clear: () => {
            dispatch({ 
                type: 'CLEAR',
             })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps
)(Main)


let ImageFile = [{
    url: 'http://montague.com.au/wp-content/uploads/2017/04/product-home-03-555x304.jpg',
    name: 'ลิ้นจี่'
}, {
    url: 'http://www.sodea-fruits.com/MTDsodea-fruit/gallery/slide-index/slide1.png',
    name: 'ทับทิม'
}, {
    url: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTHvZS4TJGKPAzXh-j0V2TZq2Qn7cB14JUc175-fMSUU3bNU51b',
    name: 'ส้ม'
}, {
    url: 'https://cdn.cnn.com/cnnnext/dam/assets/111102090930-crate-of-apples-horizontal-large-gallery.jpg',
    name: 'แอปเปิ้ล'
}, {
    url: 'https://t4.ftcdn.net/jpg/02/16/57/95/240_F_216579575_UnBkrDE7tbvtV3GAjLa6WHwvDN23ZJ0u.jpg',
    name: 'ทุเรียน'
}, {
    url: 'http://i.ndtvimg.com/i/2015-11/strawberry-625_625x350_81447914865.jpg',
    name: 'สตอเบอรี่'
}, {
    url: 'https://static.vinepair.com/wp-content/uploads/2013/10/type-of-grapes-social.jpg',
    name: 'องุ่น'
}, {
    url: 'https://assets.epicurious.com/photos/5721301fdb3cd6fd5a7dd268/16:9/w_1280,c_limit/avocados.jpg',
    name: 'อะโวคาโด'
}, {
    url: 'https://idofragrance.com/wp-content/uploads/2017/07/Banana-fragrance-05.jpg',
    name: 'กล้วย'
}, {
    url: 'http://oknation.nationtv.tv/blog/home/user_data/file_data/201311/27/670708e4e.jpg',
    name: 'แตงโม'
}, {
    url: 'https://www.bim100apco.com/images/content/original-1527756203356.jpg',
    name: 'มังคุด'
}, {
    url: 'https://i.dailymail.co.uk/i/pix/2013/05/22/article-2329246-19F1F03A000005DC-502_634x360.jpg',
    name: 'โยเกิร์ต'
},
]

// {ImageFile.map((each,index) => {
//     return(
//         <Text key={index}>each.name</Text>
//     )
// })}