import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {
    Container, Row3, MenuText, SubMenu, Topic, Row4, ProfileImageFrame,
    Content, NormalColumn, Header, ColumnEditPage, Row5, LogInImage, UserInputBox, UserInput
} from '../styledComponent'
import { connect } from 'react-redux'
class EditProduct extends Component {
    state = {
        imageURL: '',
        imageName: ''
    }

    UNSAFE_componentWillMount() {

    }
    backToProduct = () => {
        this.props.history.push('/Product')
    }
    save = () => {
        let indexx = this.props.images.imIndex
          this.props.editImage(
              this.state.imageURL,this.state.imageName,indexx)
              this.props.history.push('/Product')
    }
    render() {
        console.log(this.props)

        return (
            <Container style={{ backgroundColor: 'white' }}>
                <Header style={{ backgroundColor: '#ff1a1a' }}>
                    <SubMenu style={{ backgroundColor: '#ff1a1a', marginRight: 5 }}
                        onPress={this.backToProduct}>
                        <Text style={{ color: 'white' }}>Back</Text></SubMenu>
                    <MenuText>Edit Product</MenuText>
                </Header>


                <Row4 style={{ backgroundColor: 'white', marginTop: '5%', marginBottom: '5%' }}>

                    <ColumnEditPage style={{ height: 600 }}>
                        <ProfileImageFrame>
                            <LogInImage resizeMode="stretch" source={
                                { uri: 'https://afripoma.com/wp-content/uploads/2018/07/fix.png' }
                                // require('./login.png')
                            } />
                        </ProfileImageFrame>
                        <Row3>
                            <UserInputBox style={{ marginLeft: '8%', marginTop: '10%' }}>
                                <UserInput clear placeholder="Image Url"

                                    onChangeText={(imageURL) => this.setState({ imageURL })}></UserInput>
                                <UserInput clear placeholder="Image name"

                                    onChangeText={(imageName) => this.setState({ imageName })} ></UserInput>
                            </UserInputBox>
                        </Row3>

                        <Row3>
                            <SubMenu type="primary" style={{
                                width: '80%',
                                backgroundColor: '#ff4d4d', marginBottom: '5%',
                                marginLeft: '10%'
                            }} onPress={this.save}>
                                <Text style={{ fontSize: 25 }}>Edit</Text></SubMenu>
                        </Row3>


                    </ColumnEditPage>
                </Row4>

            </Container >
        )
    }
}
const mapStateToProps = (state) => {
    return {
        images: state.images,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        editImage: (image, imageName, imIndex) => {
            dispatch({
                type: 'EDIT_IMAGE',
                image: image,
                imageName: imageName,
                imIndex: imIndex
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps
)(EditProduct) 
