import React, { Component } from 'react';
import { Text, View, TouchableOpacity, Alert, ImageBackground } from 'react-native';
import { Button, } from '@ant-design/react-native';
import { connect } from 'react-redux'
import {
    CustomeButton, Container, Row, Row2,
    Column, UserInput, UserInputBox, LoginImageFrame, LogInImage, RowLogIn
} from '../styledComponent'


class Login extends Component {
    state = {
        username: '',
        password: '',
    }
    UNSAFE_componentWillMount() {
        this.props.addUser('aaaa', 'aaaa');
    }
    checkCorrectPass = () => {
        let user = this.state.username.toLowerCase();
        let pass = this.state.password.toLowerCase();
        console.log('user', user)
        console.log('pass', pass)
        if (user === this.props.userInfo[0].username && pass === this.props.userInfo[0].password) {
            this.props.history.push('/Main')
        } else {
            Alert.alert('InCorrect')
        }
    }

    render() {
        console.log(this.props)
        const { images, userInfo, addUser, addImage } = this.props
        return (
            <Container >
                <Row style={{backgroundColor: '#c70039'}}>
                    <LoginImageFrame>
                        <LogInImage resizeMode="stretch" source={
                            // {uri: 'https://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/4/11/1397210130748/Spring-Lamb.-Image-shot-2-011.jpg' }
                            require('./login.png')
                        } />
                    </LoginImageFrame>
                </Row>
                <Row2>
                    <Column>
                        <UserInputBox>
                            <UserInput clear placeholder="Username"
                                value={this.state.username}
                                onChangeText={(username) => this.setState({ username })}></UserInput>
                            <UserInput clear placeholder="Password"
                                value={this.state.password}
                                onChangeText={(password) => this.setState({ password })} ></UserInput>
                        </UserInputBox>
                        <CustomeButton type="primary"  onPress={this.checkCorrectPass}>Login</CustomeButton>
                    </Column>
                </Row2>
            </Container>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userInfo,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        addUser: (username, password) => {
            dispatch({
                type: 'ADD_USER',
                username: username,
                password: password
            })
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps
)(Login) 