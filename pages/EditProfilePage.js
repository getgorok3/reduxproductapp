import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {
    Container, Row3, MenuText, SubMenu, Topic, Row4, ProfileImageFrame,
    Content, NormalColumn, Header, ColumnEditPage, Row5, LogInImage, UserInputBox, UserInput
} from '../styledComponent'
import { connect } from 'react-redux'
class EditProfile extends Component {

    UNSAFE_componentWillMount() {

    }
    backToProfile = () => {
        this.props.history.push('/Profile')
    }
    save = () => {
      this.props.editUser(this.props.userInfo[0].username ,
          this.state.firstname,this.state.lastname)
          this.props.history.push('/Profile')
    }
    render() {
        console.log(this.props)
        const { userInfo } = this.props
        return (
            <Container style={{ backgroundColor: 'white' }}>
                <Header style={{ backgroundColor: '#ff1a1a' }}>
                    <SubMenu style={{ backgroundColor: '#ff1a1a', marginRight: 5 }}
                        onPress={this.backToProfile}>
                        <Text style={{ color: 'white' }}>Back</Text></SubMenu>
                    <MenuText>Edit Profile</MenuText>
                </Header>


                <Row4 style={{ backgroundColor: 'white', marginTop: '5%', marginBottom: '5%'}}>

                    <ColumnEditPage style={{ height: 600 }}>
                        <ProfileImageFrame>
                            <LogInImage resizeMode="stretch" source={
                                { uri: 'https://lh3.googleusercontent.com/NDfTVvMTt2OylnpY7GwDTGynir_919yOZZ5ZxW8GO8_8bk-LpDstR8KKC-LywHQb61k' }
                                // require('./login.png')
                            } />
                        </ProfileImageFrame>
                        <Row3>
                            <UserInputBox style={{ marginLeft: '8%', marginTop: '10%' }}>
                                <UserInput clear placeholder="First name"

                                    onChangeText={(firstname) => this.setState({ firstname })}></UserInput>
                                <UserInput clear placeholder="Last name"

                                    onChangeText={(lastname) => this.setState({ lastname })} ></UserInput>
                            </UserInputBox>
                        </Row3>

                        <Row3>
                            <SubMenu type="primary" style={{
                                width: '80%',
                                backgroundColor: '#ff4d4d', marginBottom: '5%',
                                marginLeft: '10%'
                            }} onPress={this.save}>
                                <Text style={{ fontSize: 25 }}>Edit</Text></SubMenu>
                        </Row3>


                    </ColumnEditPage>
                </Row4>
               
            </Container >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        userInfo: state.userInfo,
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        editUser: (username, firstname,lastname) => {
            dispatch({
                type: 'EDIT_USER',
                username: username,
                firstname: firstname,
                lastname: lastname
            })
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps
)(EditProfile) 