import React, { Component } from 'react';
import { View, Text, Image } from 'react-native';
import {
    Container, Row3, MenuText, SubMenu, Topic, Row4,ProductImageFrame,
    Content, NormalColumn, Header, ColumnEditPage, Row5, LogInImage
} from '../styledComponent'
import { connect } from 'react-redux'
import { WhiteSpace  } from '@ant-design/react-native';
class Product extends Component {

    UNSAFE_componentWillMount() {

    }
    backToMain = () => {
        this.props.history.push('/Main')
    }
    goToEditProduct = () => {
        this.props.history.push('/EditProduct')
    }
    render() {
        const { images } = this.props
        return (
            <Container style={{ backgroundColor: '#ff8080' }}>
                <Header style={{ backgroundColor: '#ff1a1a' }}>
                    <SubMenu style={{ backgroundColor: '#ff1a1a', marginRight: 5 }}
                        onPress={this.backToMain}>
                        <Text style={{ color: 'white' }}>Back</Text></SubMenu>
                    <MenuText>Product</MenuText>
                </Header>


                <Row4 style={{ backgroundColor: 'white', marginTop: '5%',}}>

                    <ColumnEditPage style={{height: 500}}>
                        <ProductImageFrame>
                            <LogInImage resizeMode="stretch" source={
                                {uri: images.image }
                                // require('./login.png')
                            } />
                        </ProductImageFrame>
                        <Row3>
                            <NormalColumn>
                                <Topic>Image name</Topic>
                                <Content>{images.imageName}</Content>
                                <WhiteSpace></WhiteSpace>
                                <Topic>Image url</Topic>
                                <Content>{images.image}</Content>
                            </NormalColumn>
                        </Row3>

                        {/* <Row3>
                            <NormalColumn style={{overflow: 'hidden'}}>
                                <Topic>Image url</Topic>
                                <Content>{images.image}</Content>
                            </NormalColumn>
                        </Row3> */}

                      
                    </ColumnEditPage>
                </Row4>
                <Row5 style={{
                    backgroundColor: 'white',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginBottom: '5%',
                    marginRight: '5%',
                    

                }}>
                    <SubMenu type="primary" style={{ width: '80%',
                     backgroundColor: '#ff4d4d',   marginBottom: '2%', }} onPress={this.goToEditProduct}>
                        <Text style={{fontSize: 25}}>Edit</Text></SubMenu>
                </Row5>
            </Container >
        )
    }
}

const mapStateToProps = (state) => {
    return {
        images: state.images,
    }
}

export default connect(mapStateToProps, null
)(Product) 
