import React, { Component } from 'react';
import styled from 'styled-components/native'
import { Button, InputItem } from '@ant-design/react-native';
class StyledComponent extends Component {

}
// best color deep red: 900C3F, red: c70039, yellow: FFC30F, purple 581845
export const Container = styled.View`
    flex: 1;
    backgroundColor: white;
`

export const Row = styled.View`
    flex: 1;
    flexDirection: row;
    backgroundColor: white;
    justifyContent: center;
    alignItems: center;
`

export const Row2 = styled.View`
    flex: 1;
    flexDirection: row;
    justifyContent: center;
`
export const Row3 = styled.View`
    flex: 1;
    flexDirection: row;
`
export const Row4 = styled.View`
    flex: 1;
    width: 90%;
    flexDirection: row;
    marginLeft: 5%;

`
export const Row5 = styled.View`
 
    flexDirection: row;
    marginLeft: 5%;

`
export const Column = styled.View`
    flex: 1;
    flexDirection: column;
    alignItems: center;
`
export const CustomeButton = styled(Button)`
    background-color: #FFC30F;
    border: 0;
    width: 80%;
    marginTop: 20%;
`
export const UserInput = styled(InputItem)`
    backgroundColor: white;
    color: #ff0066;
    border: 0;
`
export const UserInputBox = styled.View`
    backgroundColor: white;
    width: 80%;
`
export const LoginImageFrame = styled.View`
    margin: 5%;
    width: 500;
    height: 250;
  
`
export const LogInImage = styled.Image`
    width: 100%;
    height: 100%;
  
`
export const NewScrollView = styled.ScrollView`
    flex: 1;
`
export const Header = styled.View`
    flexDirection: row;
   
`
export const Footer = styled.View`
    flexDirection: row;
  
`
export const ImageFrame = styled.View`
    width: 50%;
    height: 100;
    backgroundColor: white;
    marginBottom: 5;
    marginRight: 5;
`
export const ImageFrameCS = styled.View`
    marginTop: 5%;
    width: 80%;
    height: 110;
    backgroundColor: white;
    marginBottom: 20;
    marginLeft: 10%;
    marginRight: 10%;
`
export const NewImage = styled.Image`
    width: 100%;
    height: 100%;
`
export const ProfileImageFrame = styled.View`
    width: 45%;
    height: 150 ;
    marginLeft: 27.5%;
    marginRight: 27.5%;
    marginTop:8%;
`
export const ProductImageFrame = styled.View`
    width: 90%;
    height: 300 ;
    marginLeft: 5%;
    marginRight: 5%;
    marginTop:8%;
`
export const MenuText = styled.Text`
    flex: 1;
    color: white;
    fontSize: 25;
    fontWeight: bold;
    textAlign: center;
    textAlignVertical: center;
`
export const SubMenu = styled(Button)`
    border: 0;
    borderRadius: 0;
`
export const ColumnEditPage = styled.View`
    flex: 1;
    flexDirection: column;
    
    
`
export const NormalColumn = styled.View`
    flex: 1;
    flexDirection: column;
    alignItems: center;
    justifyContent: center;
   
`
export const Topic = styled.Text`
    color: black;
    fontSize: 20;
    fontWeight: bold;
`

export const Content = styled.Text`
    color: black;
    fontSize: 18;
    marginLeft: 10;
`


export default StyledComponent