import React, { Component } from 'react';
import {Provider} from 'react-redux'
import {store} from './reducers/AppStore.js'
import Router from './Router'
import Test from './reducers/Test'
class App extends Component {
  render() {
  
    return (
      <Provider store={store}>
        <Router/>
        {/* <Test/> */}
      </Provider>
    );
  }
}


export default App 